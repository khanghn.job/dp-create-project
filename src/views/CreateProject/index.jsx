import React, { useState,useEffect } from 'react';
import { Layout, Alert, Space, Steps } from 'antd';
import Header from '../../container/header';
import SiderDP from '../../container/sider';
import Footer from '../../container/footer';
import BasicInformation from '../../container/basicInformation';
import './style.scss'
import ApplicationType from '../../container/applicationType';
import SourceCode from '../../container/sourceCode';
import Review from '../../container/review';
import { useDispatch,useSelector } from 'react-redux'
import {move} from '../../actions/move'
import { fetchData } from '../../actions/fetchData';
const { Sider, Content } = Layout
const { Step } = Steps
const CreartProject = () => {
    const [isScroll, setIsScroll] = useState(false)
    window.onscroll = () => {
        if (window.scrollY >90) {
            setIsScroll(true)
        } else {
            setIsScroll(false)
        }
    }


    const current = useSelector(state =>state.move.current)


    const dispatch = useDispatch()

    const onMoveNext = () => {
        dispatch(move(1))
    }
    const onMoveBack = () => {
        dispatch(move(-1))
    }
    useEffect(() => {
        dispatch(fetchData())
        
    },[])
    return (
        <Layout>
            <Header style={{ padding: 0 }}></Header>
            <Layout style={{
                padding: "24px 50px"
            }}>
                <Sider width={270}  style={{
                    backgroundColor: "transparent"
                }}>
                    <SiderDP current={current} isScroll={isScroll} />
                </Sider>

                <Content style={{
                    marginLeft: "65px"
                }}>
                    <Layout>
                        <Space
                            direction="vertical"
                            size="middle"
                            style={{
                                display: 'flex',
                            }}>
                            <Alert message={<h3 style={{
                                fontFamily: "'Lato', sans-serif"
                            }}>Project connects DevPanel to ONE Git repository</h3>}
                                description="In the project, you can create many applications. One application for each branch or tag in that repository."
                                type='info' showIcon />
                            <Steps current={ current} direction="vertical">
                                <Step title="Basic Information" description={<BasicInformation   current={ current} onMoveNext={onMoveNext}  />}></Step>
                                <Step title="Application Type" description={<ApplicationType  current={ current}  onMoveNext={ onMoveNext} onMoveBack={ onMoveBack} />}></Step>
                                <Step title="Source Code" description={<SourceCode  current={ current}  onMoveNext={ onMoveNext} onMoveBack={ onMoveBack} />}></Step>
                                <Step title="Review and Launch" description={<Review current={ current} onMoveBack={ onMoveBack} />}></Step>
                            </Steps>
                        </Space>
                    </Layout>
                </Content>
            </Layout>
            <Footer style={{ padding: 0 }}></Footer>
        </Layout>

    );
}

export default CreartProject;
