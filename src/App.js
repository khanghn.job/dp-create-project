import "./App.scss";
import CreateProject from "./views/CreateProject";
function App() {
  return (
    <div className="App">
      <CreateProject />
    </div>
  );
}
export default App;
