import logoGithub from "./logo-github.png";
import logoGitlab from "./logo-gitlab.png";
import logoBitbucket from "./bitbucket.png";
import logoDura from "./logo-drupal.png";
import logoDura8 from "./logo-drupal-8.png";
import logoDura9 from "./logo-drupal-9.png";
import logoBackdrop from "./logo-backdrop.png";
import logoMagento from "./logo-magento.png";
import iconGithub from './icon-github.png'
import iconGitlab from './icon-gitlab.png'
import iconEmty from './logo-empty.png'
export  {
    logoBackdrop,
    logoBitbucket,
    logoDura,
    logoDura8,
    logoDura9,
    logoGithub,
    logoGitlab,
    logoMagento,
    iconGithub,
    iconGitlab,
    iconEmty
}
