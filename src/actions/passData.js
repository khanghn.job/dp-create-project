import { GET_APPLICATION, PASS_DATA } from "../actionType";
export const passData = (name, description, tags,application) => {
  return {
    type: PASS_DATA,
    payload: {
      name,
      description,
      tags
    },
  };
};
export const getApp = (application) => {
  return{
    type: GET_APPLICATION,
    payload:application
  }
}
