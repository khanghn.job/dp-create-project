import { PROJECT_TYPE } from "../actionType";
export const fetchData = (data) => {
    return (
        {
            type: PROJECT_TYPE,
            payload: data
    }
)
}
export const fetchDataSuccess = (data) => {
    return (
        {
            type: "FETCH_SUCCESS",
            payload: data
        }
    )
}
export const fetchDataFail = () => {
    return (
        {
            type: "FETCH_FAIL",
        }
    )
}