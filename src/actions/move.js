import { MOVE } from "../actionType";
export const move = current => {
    return {
        type: MOVE,
        payload: current,
    }
}
