import React from 'react'
import { Radio } from 'antd'
export default function RadioButtonWraper(props) {
    const { image, name, value,key } = props
    return (
        <Radio
            value={value}
            style={{
            display: 'block',
            margin: "20px 10px"
            }}
            key={key}>
            <img src={image} alt=" " style={{width:35, marginRight:10}}/>
            <span>{name}</span>
        </Radio>
    )
}
