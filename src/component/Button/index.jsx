import React from 'react';
import { Button as AntButton } from 'antd';
import './style.scss'
const Button = props => {
    return (
        <AntButton type={props.type} className='Button' disabled={props.disabled} onClick={props.onClick} htmlType={props.htmlType} style={props.style}>
            {props.title}
        </AntButton>
    );
}

export default Button;
