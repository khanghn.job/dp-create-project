import React from 'react';
const LabelWraper = props => {
    return (
        <div style={{
            display: "block",
            margin:15
        }}>
            <b>{props.title}</b>
            <div>{props.content}</div>
        </div>
    );
}

export default LabelWraper;
