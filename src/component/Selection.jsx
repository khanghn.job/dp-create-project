import { Select, Button } from 'antd';
import React, { useState, useEffect } from 'react';
const { Option } = Select
const Selection = props => {
    const [isSelect, setIsSelect] = useState(false);
    useEffect(() => {
        setIsSelect(false)
    }, [props.name])
    return (
        <div>
            <Select
                style={{ width: "100%" }}
            >
                <Option value="link-account">
                    <Button
                        type='link'
                        style={{ color: "black" }}
                        onClick={e => {
                            setIsSelect(true)
                        }}>Link your account</Button>
                </Option>
                <Option value="use-token"><Button type='link' disabled={true}>You personal access token</Button></Option>
            </Select>
            {
                isSelect ? <Button type='primary' style={{ width: "100%", marginTop: 16 }}>
                    <img
                        src={props.src}
                        alt=" "
                        style={{
                            width: 25,
                            marginRight:10
                    }}/>
                    Connect to {props.name}
                </Button> : ""
            }
        </div>
    );
}

export default Selection;