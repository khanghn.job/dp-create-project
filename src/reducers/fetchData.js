import { PROJECT_TYPE } from "../actionType";
const initialState ={data: []}
const fetchDataReducer = (state = initialState, action) => {
    switch (action.type){
        case PROJECT_TYPE:
            return {
                ...state,
                data:action.payload
            }
        case "FETCH_SUCCESS":
            return {
                ...state,
                dataSuccess:action.payload
            }
            case "FETCH_FAIL":
                return state    
        default:
            return state
    }
}
export default fetchDataReducer;