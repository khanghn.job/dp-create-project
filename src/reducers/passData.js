import { PASS_DATA,GET_APPLICATION } from "../actionType";
const initialState = {name:"",description:"",tags:"",application:""};
const passDataReducer=(state = initialState, action) => {
  switch (action.type) {
    case PASS_DATA:
      return {
        ...state,
        name : action.payload.name,
        description : action.payload.description,
        // tags:action.payload.tags
      }
    case GET_APPLICATION:
      return {
        ...state,
        application: action.payload
}
    default:
      return state;
  }
};
export default passDataReducer