import { MOVE } from '../actionType'
const initialState={current:0}
const moveReducer= (state = initialState, action) => {
    switch (action.type) {
        case MOVE:
            return {
                ...state,
                current:state.current+action.payload
            }
        default:
            return state
    }
}
export default moveReducer