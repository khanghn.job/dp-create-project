import { combineReducers } from "redux"
import fetchDataReducer from "./fetchData"
import moveReducer from "./move"
import passDataReducer from './passData'
export default combineReducers({
    move: moveReducer,
    passData: passDataReducer,
    fetchData: fetchDataReducer
})