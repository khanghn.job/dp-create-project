import axios from "axios";
import { urlProjectType } from "../path/to/api";
import { call, put, takeLatest } from "redux-saga/effects";
import { PROJECT_TYPE } from "../actionType";
import { fetchDataFail,fetchDataSuccess } from "../actions/fetchData";

function* fetchAPI() {
  const data = yield call(async () => {
    return await (
      await axios.get(urlProjectType)
    ).data;
  });
    if (data) {
        yield put(fetchDataSuccess(data));
    } else {
        yield put(fetchDataFail())
    }
}
export default function* sagas() {
  yield takeLatest(PROJECT_TYPE, fetchAPI);
}
