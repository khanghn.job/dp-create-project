import React from 'react'
import './style.scss'
import { Header as AntHeader } from 'antd/lib/layout/layout'
export default function Header() {
  return (
    <AntHeader style={{
      backgroundColor: "var(--background-header)",
      height: "65px",
      color: "var(--font-color__white)"
    }}>
      DevPanel
    </AntHeader>
  )
}
