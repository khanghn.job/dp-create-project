import React from 'react';
import { Footer as AntFooter } from 'antd/lib/layout/layout';
const Footer = () => {
    return (
        <AntFooter style={{
            backgroundColor: "white",
            height:50
        }}>
            Dev Panel 
        </AntFooter>
    );
}

export default Footer;