import { notification, Form } from 'antd';
import React from 'react';
import { useSelector } from 'react-redux';
import { iconEmty } from '../assets';
import Button from '../component/Button';
import LabelWraper from '../component/labelWraper';


const Review = props => {
    const application = useSelector(state => state.passData.application)
    const name = useSelector(state => state.passData.name)
    const description = useSelector(state => state.passData.description)
    const tags = useSelector(state => state.passData.tags)

    const openNotificationWithIcon = (type) => {
        notification[type]({
            message: `Project ${name}`,
            description: `Tag ${tags};\t
            Description ${description} ; \t
            Type: ${application}`,
        });
    };

    return (
        <Form
            disabled={props.current !== 3}
            className='party-selection'>
            <img src={iconEmty} alt="emty" style={{ width: 35 }} />
            <LabelWraper title="Name" content={name} />
            <LabelWraper title="Description" content={description ? description : "Not set"} />
            <LabelWraper title="Tags" content={tags ? tags : "Not set"} />
            <div style={{
                display: "flex",
                justifyContent: "flex-start",
                marginTop: 20
            }}>
                <Button type='default' title="Back" onClick={props.onMoveBack} />
                <Button type='primary' title="Confirm and Create project" disabled={props.disabled} onClick={() => openNotificationWithIcon('info')} />
            </div>
        </Form>
    );
}

export default Review;
