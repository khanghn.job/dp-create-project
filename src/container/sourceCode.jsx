import { Alert, Tabs } from 'antd';
import { logoGithub, logoGitlab, logoBitbucket, iconGithub, iconGitlab } from '../assets'
import Button from '../component/Button';
import React from 'react';
import Selection from '../component/Selection';
import { Form } from 'antd';
const { TabPane } = Tabs;

const SourceCode = (props) => {
    const listSwitchs = [
        {
            key:1,
            name: 'Github',
            icon: iconGithub,
            logo: logoGithub
        },
        {
            key:2,
            name: 'Gitlab',
            icon: iconGitlab,
            logo: logoGitlab
        },
        {
            key:3,
            name: 'Bitbucket',
            icon: "",
            logo: logoBitbucket
        },
    ]
    return (
        <Form
            className='party-selection'
            id='SourceCode'
            disabled={props.current !== 2}>
            <Alert message="Please choose a git provider" type="info" style={{ marginBottom: 20 }} />
            <Tabs tabPosition="left">
                {
                    listSwitchs.map(listSwitch => {
                        return (
                            <TabPane tab={<img src={listSwitch.logo} alt={listSwitch.name} />} key={listSwitch.key} disabled={listSwitch.name==="Bitbucket"}>
                                <Selection name={listSwitch.name} src={listSwitch.icon} />
                            </TabPane>
                        )
                    })
                }
            </Tabs>
            <div style={{
                display: "flex",
                justifyContent: "flex-start",
                marginTop: 20
            }}>
                <Button type='default' title="Back" onClick={props.onMoveBack} />
                <Button type='primary' title="Next" onClick={props.onMoveNext} />
            </div>
        </Form>
    );
}

export default SourceCode;
