import React from 'react'
import { Steps, Card } from 'antd'
const { Step } = Steps
export default function SiderDP(props) {
    return (
        <>
            <Card title="Create Project" style={{
                textAlign: "center",
                fontSize: "19px",

            }} className={props.isScroll?"scroll":""}>
                <Steps progressDot current={props.current} direction="vertical" style={{
                    fontSize: "12px",
                    padding: "3px",
                }}>
                    <Step title="Basic information" />
                    <Step title="Application Type" />
                    <Step title="Source Code" />
                    <Step title="Review and Launch" />
                </Steps>
            </Card>
        </>
    )
}
