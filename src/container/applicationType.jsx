import { Radio } from 'antd'
import React, { useState } from 'react'
import RadioButtonWraper from '../component/RadioButtonWraper'
import Button from '../component/Button';
import {
    logoBackdrop,
    logoDura,
    logoDura8,
    logoDura9,
    logoMagento
} from '../assets'
import { useDispatch, useSelector } from 'react-redux';
import { getApp } from '../actions/passData';
export default function ApplicationType(props) {
    const [value, setValue] = useState(-1);

    const dispatch = useDispatch()


    const handleChangeValue = e => {
        setValue(e.target.value)
    }

    const datas = useSelector(state => state.fetchData.dataSuccess)
    return (
        <div className='party-selection' >
            <Radio.Group
                value={value}
                onChange={handleChangeValue}
                disabled={props.current !== 1}
            >
                {
                    datas ? datas.map(data => (
                        <Radio
                            value={data.id}
                            style={{
                                display: 'block',
                                margin: "20px 10px"
                            }}
                            key={data.id}>
                            <img src={logoDura} alt=" " style={{ width: 35, marginRight: 10 }} />
                            <span>{data.name}</span>
                        </Radio>
                    )) : ""
                }
            </Radio.Group>
            <div style={{
                display: "flex",
                justifyContent: "flex-start",
                marginTop: 20
            }}>
                <Button
                    type='default'
                    title="Back"
                    disabled={props.current !== 1}
                    onClick={props.onMoveBack}
                />
                <Button
                    type='primary'
                    title="Next"
                    disabled={(!(value >= 0)||props.current!==1)}
                    onClick={() => {
                        props.onMoveNext();
                        dispatch(getApp(datas[value - 1].key
                        ));

                    }} />
            </div>
        </div>

    )
}
