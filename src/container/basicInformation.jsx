import { Form, Input, Select } from 'antd';
import Button from '../component/Button';
import { useDispatch } from 'react-redux';
import { passData } from '../actions/passData'
const { Option } = Select;
const name='',description=''
const BasicInformation = props => {
    const dispatch = useDispatch()
    const [form] = Form.useForm();
    const onFinish = (values) => {
        props.onMoveNext();
        dispatch(passData(values.nameproject, values.description));
      };
    return (
        <Form
            className='basic-information party-selection'
            autoComplete="on"
            layout='vertical'
            disabled={props.current !== 0}
            onFinish={onFinish}
            form={form}
        >
            <Form.Item
                label="Name "
                name="nameproject"
                rules={[
                    {
                        required: true,
                        message: 'Please input name of project!'
                    },
                ]}
            >
                <Input  />
            </Form.Item>
            <Form.Item
                label="Description"
                name="description"
                rules={[
                    {
                        message: 'Please input name of project!'
                    },
                ]}
            >
                <Input />
            </Form.Item>
            <Form.Item
                name="Tags"
                label="Tags"
            >
                <Select
                    placeholder="Select..."
                    allowClear
                >
                    <Option value="solution" disabled={true}>No solution</Option>
                </Select>
            </Form.Item>
            <Button
                title='Next'
                type="primary"
                htmlType="submit"
                />

        </Form>
    );
}

export default BasicInformation;
